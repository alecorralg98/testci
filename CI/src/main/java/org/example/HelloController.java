package org.example;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;

public class HelloController {
    //calculator class here
    private final Calculator calculator = new Calculator();

    private boolean flag = false;

    @FXML
    private TextField textpanel;
    @FXML
    private TextField operation;

    @FXML
    protected void addNumber0() {
        calculator.addNumber(0);
        if(flag){
            textpanel.setText("0");
            operation.appendText("0");
            flag = false;
        }
        else {
            textpanel.appendText("0");
            operation.appendText("0");
        }
    }

    @FXML
    protected void addNumber1() {
        calculator.addNumber(1);
        if(flag){
            textpanel.setText("1");
            operation.appendText("1");
            flag = false;
        }
        else {
            textpanel.appendText("1");
            operation.appendText("1");
        }
    }

    @FXML
    protected void addNumber2() {
        calculator.addNumber(2);
        if(flag){
            textpanel.setText("2");
            operation.appendText("2");
            flag = false;
        }
        else {
            textpanel.appendText("2");
            operation.appendText("2");
        }
    }

    @FXML
    protected void addNumber3() {
        calculator.addNumber(3);
        if(flag){
            textpanel.setText("3");
            operation.appendText("3");
            flag = false;
        }
        else {
            textpanel.appendText("3");
            operation.appendText("3");
        }
    }

    @FXML
    protected void addNumber4() {
        calculator.addNumber(4);
        if(flag){
            textpanel.setText("4");
            operation.appendText("4");
            flag = false;
        }
        else {
            textpanel.appendText("4");
            operation.appendText("4");
        }
    }

    @FXML
    protected void addNumber5() {
        calculator.addNumber(5);
        if(flag){
            textpanel.setText("5");
            operation.appendText("5");
            flag = false;
        }
        else {
            textpanel.appendText("5");
            operation.appendText("5");
        }
    }

    @FXML
    protected void addNumber6() {
        calculator.addNumber(6);
        if(flag){
            textpanel.setText("6");
            operation.appendText("6");
            flag = false;
        }
        else {
            textpanel.appendText("6");
            operation.appendText("6");
        }
    }

    @FXML
    protected void addNumber7() {
        calculator.addNumber(7);
        if(flag){
            textpanel.setText("7");
            operation.appendText("7");
            flag = false;
        }
        else {
            textpanel.appendText("7");
            operation.appendText("7");
        }
    }

    @FXML
    protected void addNumber8() {
        calculator.addNumber(8);
        if(flag){
            textpanel.setText("8");
            operation.appendText("8");
            flag = false;
        }
        else {
            textpanel.appendText("8");
            operation.appendText("8");
        }
    }

    @FXML
    protected void addNumber9() {
        calculator.addNumber(9);
        if(flag){
            textpanel.setText("9");
            operation.appendText("9");
            flag = false;
        }
        else {
            textpanel.appendText("9");
            operation.appendText("9");
        }
    }

    @FXML
    protected void cleanCurrentNumber(){
        calculator.clearCurrentNumber();
        textpanel.setText("0");
        operation.setText("");
        flag = true;
    }

    @FXML
    protected void doOperations(){
        textpanel.setText(Integer.toString(calculator.doOperation()));
        flag = true;
        operation.setText("");
    }

    @FXML
    protected void addSymbolAddtion(){
        calculator.addSymbol("+");
        textpanel.setText("");
        operation.appendText(" + ");
        flag = true;
    }

    @FXML
    protected void addSymbolSubtraction(){
        calculator.addSymbol("-");
        textpanel.setText("");
        operation.appendText(" - ");
        flag = true;
    }

    @FXML
    protected void addSymbolMultiplication(){
        calculator.addSymbol("*");
        textpanel.setText("");
        operation.appendText(" * ");
        flag = true;
    }

    @FXML
    protected void addSymbolDivision(){
        calculator.addSymbol("/");
        textpanel.setText("");
        operation.appendText(" / ");
        flag = true;
    }

    @FXML
    protected void addSymbolKg(){
        calculator.addSymbol("Kg");
        textpanel.setText("");
        flag = true;
    }

    @FXML
    protected void addSymbolLb(){
        calculator.addSymbol("Lb");
        textpanel.setText("");
        flag = true;
    }

    @FXML
    private TextField textpanelConverter;

    @FXML
    private void openConverterLbToKg() {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("converter.fxml"));
            Parent root = loader.load();
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.setTitle("Lb to Kg Converter");
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }




}