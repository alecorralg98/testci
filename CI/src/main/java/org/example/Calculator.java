package org.example;

import java.util.ArrayList;

public class Calculator {
    private int currentNumber;
    private ArrayList<Integer> numbers;
    private String symbols;

    public Calculator(){
        resetVariables();
    }

    public void addNumber(int number){
        currentNumber = (currentNumber * 10) + number;
    }

    public void addSymbol(String newSymbol){
        symbols += newSymbol;
        numbers.add(currentNumber);
        clearCurrentNumber();
    }

    public void clearCurrentNumber(){
        currentNumber = 0;
    }

    public int doOperation(){
        int finalNumber;
        //adds last number
        numbers.add(currentNumber);
        //checks if user didnt try to blow this up
        if(numbers.size() > 1){
            //gets first number
            finalNumber = numbers.get(0);
            //goes through the array and the symbols to do operations
            for (int i = 1; i < numbers.size(); i++) {
                char operation = symbols.charAt(i - 1);
                switch (operation) {
                    case '+' -> finalNumber += numbers.get(i);
                    case '*' -> finalNumber *= numbers.get(i);
                    case '-' -> finalNumber -= numbers.get(i);
                    case '/' -> {
                        if (numbers.get(i) != 0) {
                            finalNumber /= numbers.get(i);
                        } else {
                            // Handle division by zero
                            throw new ArithmeticException("Division by zero is not allowed.");
                        }
                    }
                }
            }
        }
        //if user just pressed equals
        else{
            finalNumber = currentNumber;
        }
        //reset to be ready for next set of operations
        resetVariables();
        return finalNumber;
    }

    private void resetVariables(){
        clearCurrentNumber();
        symbols = "";
        numbers = new ArrayList<>();
    }
}
